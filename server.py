import asyncio
from multiprocessing import Process
import random
from threading import Thread
from flask import Flask, jsonify, request, redirect, send_file
import generator
from flask_cors import CORS
from generator.id import get_random_id
from generator.physics_lambda import generate_physics_lambda
from generator.ring_placer import RingPlacer
from generator.run_job import run_job
import subprocess
app = Flask(__name__)
app.debug = True

CORS(app)


#Establishes HTML
#TODO This is almost definitely really bad practice. Fix this when possible
form = ''
with open('./resources/input-web.html','r') as file:
    form = file.read()



@app.route("/")
def return_form():
    return form

@app.route("/generate-microstructure", methods=['POST'])
def generate_microstructure():

    params = request.get_data()
    print(params)
    
    radius_mean = str(request.form.get('mean-radius'))
    radius_std = str(request.form.get('std-radius'))

    print(radius_mean, radius_std)
    
    
    id = get_random_id()
    
    subprocess.run(['python','generate.py', id, radius_mean, radius_std])

    response_data = {'id': id}
    return jsonify(response_data), 200    



@app.route("/microstructure/<id>")
def get_microstructure_data(id):
    out = ''
    with open('./resources/output-web.html','r') as file:
        out = file.read()
    return out

@app.route('/microstructure/matrix/<id>')
def get_matrix_image(id):
    filename = './jobs/'+id+'/material_matrix.png'
    return send_file(filename, mimetype='image/png')

@app.route('/microstructure/fgm-output/<id>')
def get_fgm_output(id):
    filename = './jobs/'+id+'/material_output.out'
    return send_file(filename)

@app.route('/microstructure/neighbor-output/<id>')
def get_neighbor_output(id):
    filename = './jobs/'+id+'/neighbor_distribution.png'
    return send_file(filename, mimetype='image/png')

@app.route('/microstructure/angle-output/<id>')
def get_angle_output(id):
    filename = './jobs/'+id+'/angle_distribution.png'
    return send_file(filename, mimetype='image/png')

app.run(port = 8080)