import random
import sys
from generator.run_job import run_job
from generator.physics_lambda import generate_physics_lambda

from generator.ring_placer import RingPlacer

args = sys.argv

id = args[1]
m_r = float(args[2])
std_r = float(args[3])
num_rings = int(args[4])
width = int(args[5])
height = int(args[6])

print(args)

height = 100
percent = 1


ring_placer = RingPlacer(width,height)
ring_placer.ring_inner_lambda = lambda n: percent*random.normalvariate(m_r,std_r)
ring_placer.ring_outer_lambda = lambda n, in_r: in_r/percent
#TODO Make this variable on input parameters
ring_placer.ring_placement_lambda = generate_physics_lambda(1.9,0.1,0.1,1,4)

run_job(id, ring_placer, num_rings, [2], wrapped = True)