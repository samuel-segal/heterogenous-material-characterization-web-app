from math import copysign, sqrt
import random
from typing import List, Tuple
from generator.ring import Ring
import math

def mag_sq(vec):
    return vec[0]*vec[0]+vec[1]*vec[1]

def mag(vec):
    return sqrt(mag_sq(vec))



#Makes the rings fall in a physical fashion
def physics_lambda(
    width: int, height: int,
    n: int, rings: List[Ring],
    in_r: float,
    out_r: float,
    ghost_radius_multiplier: float,
    dxdn: float = 0.1,
    dydn: float = 0.1,
    outbox_x: float = 4,
    outbox_y: float = 4) -> Tuple[int,int]:

    ghost_radius = out_r * ghost_radius_multiplier
    print(n)
    #Finds the highest the ball can drop
    px = random.uniform(0, width)
    #Drops down until it is no longer touching anything
    py = 0

    semi_r = (0.7*ghost_radius+0.3*out_r)
    is_colliding=True
    while is_colliding:
        
        is_colliding = False
        for ring in rings:
            if math.floor(ring.y - ring.out_r-1)> height:
                continue
            if ring.intersects_ring_wrapped(
                x=px,
                y=py,
                out_r=semi_r,
                width=width,
                height=height
            ):
                is_colliding = True
                break
        if py >= height:
            return None
        py += dydn
    


    terminate = 30

    t = 0

    past_ring = None

    #If spawn is already occupied, it gets rid of it
    #This shouldn't occur, it's just a failsafe
    for ring in rings:
        if ring.y-ring.out_r < height and ring.intersects_ring_wrapped(
                x = px,
                y = py,
                out_r=semi_r,
                width= width,
                height=height,
                wrapY = True
                ):
            return None

    #Until it hits the bottom or sooner
    while py+out_r+1 < height+outbox_y*out_r and px-out_r > - outbox_x*out_r and px+out_r < width+outbox_x*out_r and  t<terminate:
        
        collide_ring = None
        #Checks if there is a ring below
        for ring in rings:
            if ring.intersects_ring_wrapped(
                x = px,
                y = py,
                out_r=ghost_radius,
                width= width,
                height=height,
                wrapY = False
                ):
                collide_ring = ring
                break
        
        if collide_ring:
            if collide_ring != past_ring:
                t += 1
                past_ring = collide_ring
        
            dx = px - collide_ring.x
            if abs(dx) > width/2:
                dx *=-1
            
            #To prevent standing grass stalks from never completing
            if dx ==0: break
            while collide_ring.intersects_ring_wrapped(px,py,ghost_radius, width, height, wrapY=False):
                px += copysign(dxdn,dx)   
        else:
            py += dydn

    
    return (px,py)

def generate_physics_lambda(ghost_radius_multiplier:float, dxdn: float, dydn: float, outbox_x: float, outbox_y: float):
    def out_function(width: int, height: int, n: int, rings: List[Ring],in_r: float,out_r: float):
        return physics_lambda(width,height,n,rings,in_r,out_r,ghost_radius_multiplier,dxdn,dydn,outbox_x,outbox_y)
    return out_function