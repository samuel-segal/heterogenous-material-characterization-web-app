import matlab.engine
import os
from generator.file_data_packager import package_output

from generator.material_buffer import MaterialBuffer


#TODO Implement this but without filing
def get_matlab_analysis(material_buffer: MaterialBuffer,destination_folder: str, loading_parameter=1):

    file_out = package_output(material_buffer)

    input_file_name = os.path.abspath(destination_folder+'material_input.fgm')
    with open(input_file_name,'w') as file:
        file.write(file_out)

    eng = matlab.engine.start_matlab()

    eng.clearvars(nargout=0)
    eng.close('all',nargout=0)
    eng.clc(nargout=0)

    eng.workspace['file_input_title'] = input_file_name
    eng.workspace['file_output_title']= os.path.abspath(destination_folder+'material_output_'+str(loading_parameter)+'.out')
    eng.workspace['string_title']=os.path.abspath(destination_folder+'matlab_data')
    eng.workspace['LOP']=loading_parameter

    eng.cd('matlab-src')
    eng.fvdam_global_exec(nargout=0)
