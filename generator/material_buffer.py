from PIL import Image

#Defined class for output "image" representing material binaries
class MaterialBuffer:
    def __init__(self,width: int,height: int,default_material: int) -> int:
        self.width = width
        self.height = height
        self.default_material = default_material
        if not(default_material):
            default_material = 0

        #C might be more suitable for this purpose
        self.arr = [ [default_material]*width for _ in range(height)]
        
    #Sets the material at a given point
    def set_material(self,x:int ,y:int,material:int) -> int:
        self.arr[y][x] = material

    #Gets the material at a given point
    def get_material(self,x:int,y:int) -> int:
        return self.arr[y][x]
    
    #Gets the volume fraction of the material
    def get_body_fraction(self):
        out = 0
        for row in self.arr:
            for n in row:
                if n==self.default_material:
                    out+=1
        return 1 - out/ (self.width*self.height)

    def __str__(self):
        out = ''
        for index, row in enumerate(self.arr):
            out = out + format(index+1,'<5d') + '   ' + ' '.join(str(x) for x in row) + '\n'

        return out
    
        #Saves material buffer text to a file
    def save_text(self, filename: str) -> None:
        with open(filename,'w') as file:
            file.write(str(self))

    #Saves material buffer as an image, with each different material's pixel color being defined in the color_map dictionary
    def save_image(self,filename: str,color_map: dict[int,str]) -> None:
        img = Image.new('RGB',[self.width,self.height], color='white')
        pixels = img.load()

        for x in range(self.width):
            for y in range(self.height):
                material = self.get_material(x,y)
                pixels[x,y] = color_map[material]
        
        img.save(filename)
