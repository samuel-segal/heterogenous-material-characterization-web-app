from generator.matlab_connector import get_matlab_analysis
from generator.ring_placer import RingPlacer
import matplotlib.pyplot as plt
import os
import math
import time


curr_time = 0
def establish_time():
    global curr_time
    curr_time = time.time()

def get_time_diff():
    now = time.time()
    global curr_time
    time_diff = now - curr_time
    curr_time = now
    return time_diff

#Similar to numpy implementation
def linspace(start: float, stop: float, n: int):
    return [ start + i*(stop-start)/n for i in range(n+1)]

def polar_hist(data):
    dN = 10
    #Gets the histogram values
    n_bin = [0 for _ in range(360//dN)]
    for value in data:
        b = int(value // dN)
        n_bin[b] += 1
    
    #Makes the bins relative
    for i in range(len(n_bin)):
        n_bin[i] = n_bin[i]/len(data)

    theta = list(map(math.radians,range(0,360,dN)))
    tick_label = range(0,360, dN)
    width = [math.radians(dN) for _ in range(360//dN)]
    colors = plt.cm.viridis(n_bin)
    ax = plt.subplot(projection='polar')
    ax.bar(theta, n_bin, bottom=0, color=colors, width=width, tick_label = tick_label)

def run_job(job_id: str, ring_placer: RingPlacer, num_rings: int, lop_arr=[1], wrapped=False):
    #Creates appropriate folder for ID
    folder_name = './jobs/'+job_id+'/'
    os.mkdir(folder_name)

    start_time = time.time()
    #Establishes initial time
    establish_time()

    #Performs ring calculations
    ring_placer.generate_rings(num_rings)
    placement_time = get_time_diff()

    #Generates material matrix
    material_buffer = ring_placer.generate_material_matrix(1,2,2,wrapped=wrapped)
    volume_fraction = material_buffer.get_body_fraction()
    buffer_time = get_time_diff()

    material_buffer.save_image(folder_name+'input.png',{1: 0, 2: (255,199,126), 3: (255,234,207)})

    #Saves plot of average neighbors, set to 1.05 for rounding errors
    average_neighbors = ring_placer.get_neighbor_distribution(3, wrapped = wrapped)
    plt.hist(average_neighbors,bins=range(0,10,1),density=True)
    plt.xticks(range(0,10,1))
    plt.title('Average Neighbors')
    plt.savefig(folder_name+'neighbor_distribution.png')
    plt.clf()

    #Saves plot of average neighbor angle distribution, set to 1.05 for rounding errors
    average_angle_neighbors = ring_placer.get_neighbor_angle_distribution(2.5, wrapped = wrapped)
    angle_rounded = list(map(round,average_angle_neighbors))
    plt.hist(angle_rounded,bins=range(0,360,30),density=True)
    plt.title('Average Angle Neighbors')
    plt.savefig(folder_name+'angle_distribution.png')
    plt.clf()

    polar_hist(angle_rounded)
    plt.title('Average Angle Neighbors')
    plt.savefig(folder_name+'angle_distr_polar.png')
    plt.clf()

    dist_average = ring_placer.get_neighbor_distance_average(2.5, wrapped=wrapped)
    plt.hist(dist_average, bins= linspace(0,max(dist_average),10),density=True)
    plt.title('Average Neighbor Distance')
    plt.savefig(folder_name+'neighbor_distance_avg.png')
    plt.clf()

    dist_spread = ring_placer.get_neighbor_distance_spread(2.5, wrapped=wrapped)
    plt.hist(dist_spread, bins= linspace(0,max(dist_spread),10), density=True)
    plt.title('Spread Neighbor Distance')
    plt.savefig(folder_name+'neighbor_distance_spread.png')
    plt.clf()

    #Saves image of material matrix
    plt.imshow(material_buffer.arr,cmap='copper')
    plt.title('Material Matrix')
    plt.savefig(folder_name+'material_matrix.png')
    plt.clf()

    anl_file = folder_name+'anl_out.txt'
    with open(anl_file,'w') as out_file:
        out_file.write('Placement Time: '+str(placement_time)+'s\n')
        out_file.write('Buffer Time: '+str(buffer_time)+'s\n')
        out_file.write('Volume Fraction: '+str(volume_fraction)+'\n')
        out_file.write('Neighbors: ')
        out_file.write(str(average_neighbors)+'\n')
        out_file.write('Angle Neighbors: ')
        out_file.write(str(average_angle_neighbors)+'\n')
        out_file.write('Neighbor Distance Average: ')
        out_file.write(str(dist_average)+'\n')
        out_file.write('Neighbor Distance Spread ')
        out_file.write(str(dist_spread)+'\n')

    #Packages file
    establish_time()
    with open(anl_file,'a') as out_file:
        for lop in lop_arr:
            get_matlab_analysis(material_buffer,folder_name,lop)
            new_time = get_time_diff()
            out_file.write('LOP '+str(lop)+' Time: '+str(new_time)+'s\n')
        
        end_time = time.time()
        out_file.write('Total Time: '+str(end_time-start_time)+'s\n')