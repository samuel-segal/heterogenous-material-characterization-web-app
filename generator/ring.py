#Defined class for geometric configuration of ring.
from math import dist, sqrt


class Ring:
    def __init__(self, x: float, y: float, in_r: float, out_r: float):
        self.x = x
        self.y = y
        self.in_r = in_r
        self.out_r = out_r

    #Returns boolean according to if the point lies on the ring itself    
    def is_on_ring(self,x: float,y: float) -> bool:
        dx = x - self.x
        dy= y - self.y
        rsq = dx*dx + dy*dy

        return (rsq >= self.in_r*self.in_r) and (rsq <= self.out_r*self.out_r)

    #Returns boolean according to if the point lies outside the ring
    def is_outside_ring(self,x: float,y: float) -> bool:
        dx = x - self.x
        dy= y - self.y
        rsq = dx*dx + dy*dy

        return rsq > (self.out_r*self.out_r)
    
    #Returns boolean according to if the point lies within the ring
    def is_inside_ring(self,x: float,y: float) -> bool:
        dx = x - self.x
        dy= y - self.y
        rsq = dx*dx + dy*dy

        return rsq < (self.in_r * self.in_r)

    #Returns boolean according to if self object intesects with other ring defined here
    def intersects_ring(self, x,y,out_r) -> bool:
        r_tot = self.out_r + out_r
        dx = self.x - x
        dy = self.y - y

        return (dx*dx+dy*dy) <= r_tot*r_tot
    
    #Measures the distance from the outside of self ring and external ring defined here
    def ring_distance(self, x,y,out_r) -> bool:
        r_tot = self.out_r  + out_r

        return dist((self.x,self.y),(x,y)) - r_tot
    
    #TODO
    def is_on_ring_wrapped(self, x, y, width, height, wrapX = True, wrapY = True):
        rsq = self.__wrapped_dist_sq(x,y, width, height, wrappedX=wrapX, wrappedY=wrapY)
        return (rsq >= self.in_r*self.in_r) and (rsq <= self.out_r*self.out_r)
        
    #TODO
    def is_outside_ring_wrapped(self, x, y, width, height, wrapX = True, wrapY = True):
        rsq = self.__wrapped_dist_sq(x,y, width, height, wrappedX=wrapX, wrappedY=wrapY)
        return rsq > (self.out_r*self.out_r)

    
    #TODO
    def is_inside_ring_wrapped(self, x, y, width, height, wrapX = True, wrapY = True):
        rsq = self.__wrapped_dist_sq(x,y, width, height, wrappedX=wrapX, wrappedY=wrapY)
        return rsq < (self.in_r * self.in_r)
    

    def intersects_ring_wrapped(self, x,y,out_r , width, height, wrapX = True, wrapY = True) -> bool:
        r_tot = self.out_r + out_r
        rsq = self.__wrapped_dist_sq(
            x=x,
            y=y,
            width=width,
            height=height,
            wrappedX=wrapX,
            wrappedY=wrapY
        )
        return rsq <= r_tot*r_tot
    
    #Measures the distance from the outside of self ring and external ring defined here
    def ring_distance_wrapped(self, x,y,out_r, width, height, wrapX = True, wrapY = True) -> bool:
        r_tot = self.out_r  + out_r
        rsq = self.__wrapped_dist_sq(x,y, width, height, wrappedX=wrapX, wrappedY=wrapY)
        
        return sqrt(rsq) - r_tot
    
    def __wrapped_dist_sq(self, x, y, width, height, wrappedX = True, wrappedY = True):
        dx = abs(self.x - x)
        if wrappedX and dx > width/2:
            dx -= width
        dy = abs(self.y -y)
        if wrappedY and dy > height/2:
            dy -= height
        
        return dx*dx + dy*dy

    def __str__(self) -> str:
        return '<('+str(self.x)+','+str(self.y)+') (i'+str(self.in_r)+' o'+str(self.out_r)+')>'