from typing import Callable, List, Tuple
from generator.material_buffer import MaterialBuffer
from generator.ring import Ring
import math

class RingPlacer:
    def __init__(self,width: int, height: int):
        self.width = width
        self.height = height

        self.ring_list: list[Ring] = []

        #Initializes default lambda placements
        
        #Lambda function representing ring inner radius given n-th ring in material buffer mb, and center variables x and y
        #Returns scalar representing ring inner radius
        self.ring_inner_lambda: Callable[[int],float]= lambda n: 1
        
        #Lambda function representing ring inner radius given n-th ring in material buffer mb, and center variables x and y
        #Returns scalar representing ring outer radius
        self.ring_outer_lambda: Callable[[int,int],float] = lambda n, in_r: 2

        #Lambda function representing ring center placement given n-th ring in material buffer mb, given inner radius of ring and outer radius
        #Returns ordered pair representing center position
        self.ring_placement_lambda: Callable[[int,int,int,List[Ring],float,float],Tuple[int,int]] = lambda width, height, n, rings, inner_r, outer_r: (6*(n%2),6*int(n/2))

        #Lambda function representing whether or not a single "pixel" of ring will show up or not: given n-th ring in material buffer mb,
        #ring object ring, and x and y value of pixel
        #Returns boolean value representing whether or not the given pixel has its material changed
        self.placement_bool_lambda: Callable[[Ring,float,float],bool] = lambda ring, x, y: True

        #Lambda function representing whether or not a single "pixel" of pulp inside ring will appear or not, given n-th ring in material buffer mb,
        #ring object ring, and x and y value of pixel
        #Returns boolean value representing whether or not the given pixel has its material changed
        self.inner_placement_bool_lambda: Callable[[Ring,float,float],bool] = lambda ring, x, y: True

    #Generates rings according to how the user defined them previously.
    def generate_rings(self, n_tot: int) -> None:
        self.ring_list = [];

        for n in range(n_tot):
            ring_in_r = self.ring_inner_lambda(n)
            ring_out_r = self.ring_outer_lambda(n, ring_in_r)
            pos_out = self.ring_placement_lambda(self.width, self.height, n, self.ring_list, ring_in_r, ring_out_r)
            if not pos_out:
                continue
            ring_x, ring_y = pos_out

            r = Ring(ring_x,ring_y,ring_in_r,ring_out_r)
            
            self.ring_list.append(r)
    
    #Generates the matrix according to the given ring positions
    def generate_material_matrix(self, default_material: int, sheath_material: int, pulp_material: int, wrapped=False):
        if not wrapped:
            return self.__generate_unwrapped_matrix(
                default_material=default_material,
                sheath_material=sheath_material,
                pulp_material=pulp_material
            )
        else:
            return self.__generate_wrapped_matrix(
                default_material=default_material,
                sheath_material=sheath_material,
                pulp_material=pulp_material
            )
        
    def get_neighbor_distribution(self, maxDistanceMultiplier, countOutside = False, wrapped = False):
        if wrapped:
            return self.__get_neighbor_wrapped(maxDistanceMultiplier)
        else:
            return self.__get_neighbor_unwrapped(maxDistanceMultiplier, countOutside=countOutside)        
        
    def get_neighbor_angle_distribution(self, maxDistanceMultiplier, countOutside=False, wrapped = False):
        if wrapped:
            return self.__get_angle_wrapped(maxDistanceMultiplier)
        else:
            return self.__get_angle_unwrapped(maxDistanceMultiplier, countOutside=countOutside)
        
    def get_neighbor_distance_average(self, maxDistanceMultiplier, countOutside=False, wrapped = False ):
        if wrapped:
            return self.__get_dist_avg_wrapped(maxDistanceMultiplier)
        else:
            print('not implemented yet')

    def get_neighbor_distance_spread(self, maxDistanceMultiplier, countOutside=False, wrapped = False):
        if wrapped:
            return self.__get_dist_spread_wrapped(maxDistanceMultiplier)
        else:
            print('not implemented yet')

    def __generate_unwrapped_matrix(self, default_material, sheath_material, pulp_material):
        material_buffer: MaterialBuffer = MaterialBuffer(self.width, self.height, default_material)
        for ring in self.ring_list:
            #Establishes limits for loop
            min_x = max(0,int(ring.x-ring.out_r-1))
            max_x = min(self.width,int(ring.x+ring.out_r+1))
            min_y = max(0,int(ring.y-ring.out_r-1))
            max_y = min(self.height,int(ring.y+ring.out_r+1))

            for x in range(min_x,max_x):
                for y in range(min_y,max_y):
                    if(ring.is_on_ring(x,y)):
                        #Ensures that the square is empty
                        if material_buffer.get_material(x,y) == material_buffer.default_material:
                            #Does check specific to individual ring placer lambda
                            is_valid = self.placement_bool_lambda(ring,x,y)
                            if is_valid:
                                #Places material down at x,y
                                material_buffer.set_material(x,y,sheath_material)
                    elif ring.is_inside_ring(x,y):
                            is_valid = self.inner_placement_bool_lambda(ring,x,y)
                            if is_valid:
                                #Places material down at x,y
                                material_buffer.set_material(x,y,pulp_material)
        return material_buffer
    
    #Returns a list of visual cylinders
    def __get_interior_rings_unwrapped(self):
        return list(filter(
            lambda r: r.x> -r.out_r and r.x< self.width+r.out_r and r.y> -r.out_r and r.y < self.height+r.out_r,
            self.ring_list
        ))
    
    def __unwrapped_neighbors(self, ring, maxDistanceMultiplier):
        out_rings = []
        for r in self.ring_list:
            is_intersect = ring.intersects_ring(r.x,r.y, r.out_r+maxDistanceMultiplier*ring.out_r)
            if is_intersect and r != ring:
                out_rings.append(ring)
        return out_rings

    def __get_neighbor_unwrapped(self, maxDistanceMultiplier, countOutside = False):
        out_arr = []
        ringList = self.ring_list if countOutside else self.__get_interior_rings_unwrapped()
        for ring in ringList:
            tot = 0
            for r in self.ring_list:
                is_intersect = ring.intersects_ring(r.x,r.y,r.out_r+maxDistanceMultiplier*ring.out_r)
                if is_intersect:
                    tot +=1
            #-1 because otherwise it would count itself as a neighbor
            out_arr.append(tot-1)
        return out_arr

    def __get_angle_unwrapped(self, maxDistanceMultiplier, countOutside=False):
        out_arr = []
        ringList = self.ring_list if countOutside else self.__get_interior_rings_unwrapped()
        for ring in ringList:
            neighbors = self.__unwrapped_neighbors(ring, maxDistanceMultiplier)
            angles = list(map(lambda r: math.degrees(math.atan2(r.y-ring.y,r.x-ring.x)) % 360, neighbors))
            angles.sort()

            out_angles = []
            for i in range(len(angles)):
                n1 = i
                n2 = (i+1) % len(angles)
                
                ng1 = angles[n1] % 360
                ng2 = angles[n2] % 360

                dNg = abs((ng2-ng1) % 360)
                dNg2 = abs((ng1-ng2) % 360)

                out_angles.append(min(dNg,dNg2)) 
            out_arr.extend(out_angles)

        return out_arr
    
    

    def __generate_wrapped_matrix(self, default_material, sheath_material, pulp_material):
        material_buffer = MaterialBuffer(self.width, self.height, default_material)
        for ring in self.ring_list:
            min_x = math.floor(ring.x-ring.out_r-1)
            min_y = math.floor(ring.y - ring.out_r-1)
            max_x = math.ceil(ring.x + ring.out_r + 1)
            max_y = math.ceil(ring.y + ring.out_r + 1)
            #Checks if ring is out of bounds
            if min_x < 0 and max_x < 0:
                continue
            if min_x > self.width and max_x > self.width:
                continue
            if min_y < 0 and max_y < 0:
                continue
            if min_y  > self.height and max_y > self.height:
                continue
            for x in range(min_x,max_x):
                for y in range(min_y,max_y):
                    tx = x % self.width
                    ty = y % self.height
                    if ring.is_on_ring_wrapped(x = tx, y = ty, width=self.width, height=self.height):
                        is_valid = self.placement_bool_lambda(ring,tx,ty)
                        if is_valid:
                            material_buffer.set_material(tx,ty, sheath_material)
                    elif ring.is_inside_ring_wrapped(tx,ty, self.width, self.height):
                        is_valid = self.inner_placement_bool_lambda(ring,tx,ty)
                        if is_valid:
                            #Places pulp material at x,y
                            material_buffer.set_material(tx,ty,pulp_material)
        return material_buffer

    def __get_interior_rings_wrapped(self):
        return list(filter(
            lambda r: r.x >= 0 and r.x <= self.width and r.y >= 0 and r.y <= self.height,
            self.ring_list
        ))
    
    def __wrapped_neighbors(self, ring, maxDistanceMultiplier):
        out_rings = []
        ringList = self.__get_interior_rings_wrapped()
        for r in ringList:
            is_intersect = ring.intersects_ring_wrapped(r.x, r.y,maxDistanceMultiplier*ring.out_r,self.width, self.height)
            if is_intersect and r != ring:
                out_rings.append(r)
        return out_rings
    
    def __get_neighbor_wrapped(self, maxDistanceMultiplier):
        out_arr = []
        ringList = self.__get_interior_rings_wrapped()
        for ring in ringList:
            neighbors = self.__wrapped_neighbors(ring, maxDistanceMultiplier)
            out_arr.append( len(neighbors) )
        return out_arr
    
    def __get_angle_wrapped(self, maxDistanceMultiplier):
        out_arr = []
        ringList = self.__get_interior_rings_wrapped()
        for ring in ringList:
            angles = []
            neighbors = self.__wrapped_neighbors(ring, maxDistanceMultiplier)
            for r in neighbors:
                dy = r.y - ring.y
                dx = r.x - ring.x

                if dy > self.height/2:
                    dy = dy - self.height
                elif dy < -self.height/2:
                    dy = dy + self.height
                
                if dx > self.width/2:
                    dx = dx - self.width
                elif dx < -self.width/2:
                    dx = dx + self.width



                abs_angle = math.degrees(
                    math.atan2(
                        dy,dx
                    )
                ) % 360
                angles.append(abs_angle)
            angles.sort()
            out_angles = []
            for i in range(len(angles)):
                n1 = i
                n2 = (i+1) % len(angles)

                ng1 = angles[n1] % 360
                ng2 = angles[n2] % 360

                dNg = abs((ng2-ng1) % 360)
                dNg2 = abs((ng1-ng2) % 360)

                out_angles.append(min(dNg,dNg2))
            
            out_arr.extend(out_angles)
        return out_arr
    
    
    def __get_dist_avg_wrapped(self, maxDistanceMultiplier):
        out_arr = []
        ringList = self.__get_interior_rings_wrapped()
        for ring in ringList:
            neighbors = self.__wrapped_neighbors(ring, maxDistanceMultiplier)
            wrapped_dist = list()
            for r in neighbors:
                wrapped_dist.append(
                    ring.ring_distance_wrapped(r.x,r.y,r.out_r,self.width,self.height)
                )
            if len(wrapped_dist):
                avg = sum(wrapped_dist) / len(wrapped_dist)
                out_arr.append(avg)
        return out_arr

    def __get_dist_spread_wrapped(self, maxDistanceMultiplier):
        out_arr = []
        ringList = self.__get_interior_rings_wrapped()
        for ring in ringList:
            neighbors = self.__wrapped_neighbors(ring, maxDistanceMultiplier)
            wrapped_dist = list()
            for r in neighbors:
                wrapped_dist.append(
                    ring.ring_distance_wrapped(r.x,r.y,r.out_r,self.width,self.height)
                )
            if len(wrapped_dist):
                spread = max(wrapped_dist) - min(wrapped_dist)
                out_arr.append(spread)
        return out_arr