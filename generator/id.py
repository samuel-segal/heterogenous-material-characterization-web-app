from datetime import datetime
from random import randbytes

def get_random_id():
    now=  datetime.now()
    date_str = '-'.join(list(map(str,[now.year,now.month,now.day,now.hour,now.minute,now.second])))
    return 'GDMM_'+date_str+'_'+str(randbytes(5).hex())