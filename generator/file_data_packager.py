from generator.material_buffer import MaterialBuffer

template = ''
with open('./resources/fvdam-template.txt','r') as template_file:
    template = str(template_file.read())

def package_output(material_buffer: MaterialBuffer):
    assign_const = 0.00077160

    dim_fix = template.replace('%height_dim',str(material_buffer.height)).replace('%width_dim',str(material_buffer.width))
    height_assignment: str = '\n'.join(list(map( lambda i: str(i+1)+'     '+str(assign_const), range(material_buffer.height))))
    width_assignment: str = (str(assign_const)+' ')*material_buffer.width
    assigment_fix = dim_fix.replace('%height_assignments',height_assignment).replace('%width_assignments',width_assignment)
    


    #TODO Implement
    mb_lines = material_buffer.arr
    mb_numbered = []
    for idx, line in enumerate(mb_lines):
        new_line = str(len(mb_lines)-idx)+'     '+' '.join(map(str,line))
        mb_numbered.append(new_line)
    out_mat = '\n'.join(mb_numbered)
    out_str = assigment_fix.replace('%material_matrix',out_mat)
    return(out_str)