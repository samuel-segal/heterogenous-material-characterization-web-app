import random

def cubic(distance, num_columns):
    return ( lambda width,height,n,rings,in_r,out_r: ( distance*(n%num_columns-1), distance*(int(n/num_columns)-1) ) )

def hexagonal(distance, num_columns):
    return lambda width, height, n, rings, in_r, out_r: ( distance*(n%num_columns-2+( (int(n/num_columns)%2) / 2)), distance*(int(n/num_columns)-2) )

def random_distr():
    return lambda width,height,n,rings, in_r, out_r: (width*random.random(), height * random.random())